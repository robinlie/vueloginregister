import Vue from "vue";
import VeeValidate from "vee-validate";

import { store } from "./components/_store";
import { router } from "./components/_helpers";
import App from "./app/App.vue";

Vue.config.productionTip = false;

Vue.use(VeeValidate);

import { configureFakeBackend } from "./components/_helpers";
configureFakeBackend();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
