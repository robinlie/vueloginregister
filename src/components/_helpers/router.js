import Vue from "vue";
import VueRouter from "vue-router";

import HomePage from "../../home/HomePage";
import LoginPage from "../../login/LoginPage.vue";
import RegisterPage from "../../register/RegisterPage";

Vue.use(VueRouter);

const routes = [
  { path: "/", component: HomePage },
  { path: "/login", component: LoginPage },
  { path: "/register", component: RegisterPage },

  { path: "*", redirect: "/" }
];

export const router = new VueRouter({
  mode: "history",
  routes
});

router.beforeEach((to, from, next) => {
  // to and from are both route objects. must call `next`.
  const publicPages = ["/login", "/register"];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem("user");

  if (authRequired && !loggedIn) {
    return next("/login");
  }

  next();
});
